package com.company.PrimaryAttributes;

public class PrimaryAttribute {
    // Declarations
    protected int vitality;
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    // Constructor
    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence){
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }


    // Getters and setters
    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    // Custom toString
    @Override
    public String toString() {
        return "{" +
                "vitality=" + vitality +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }

    // Overriding equals to check data-equality, not object-equality
    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        // Check if o is an instance of PrimaryAttribute or not "null instanceof [type]" also returns false
        if (!(o instanceof PrimaryAttribute)) {
            return false;
        }

        // typecast o to PrimaryAttribute so that we can compare data members
        PrimaryAttribute c = (PrimaryAttribute) o;

        // Compare the data members and return accordingly
        return Integer.compare(vitality, c.vitality) == 0
                && Integer.compare(strength, c.strength) == 0
                && Integer.compare(dexterity, c.dexterity) == 0
                && Integer.compare(intelligence, c.intelligence) == 0;
    }
}
