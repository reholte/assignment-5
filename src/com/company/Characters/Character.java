package com.company.Characters;

import java.util.HashMap;
import java.util.List;

import com.company.Exceptions.InvalidArmorException;
import com.company.Exceptions.InvalidSlotException;
import com.company.Exceptions.InvalidWeaponException;
import com.company.Items.Item;
import com.company.Items.ItemType.Armor;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.Weapon;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;
import com.company.Slots.Slot;

public abstract class Character {
    // Declarations
    protected String name;
    protected int level;
    protected PrimaryAttribute basePrimaryAttribute;
    protected PrimaryAttribute totalPrimaryAttribute;
    protected HashMap<Slot, Item> equipment;
    protected List<WeaponTypes> usableWeapons;
    protected List<ArmorTypes> usableArmors;
    protected double DPS;


    // Getters and setters
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public double getDPS() {
        return DPS;
    }

    public PrimaryAttribute getTotalPrimaryAttribute() {
        return totalPrimaryAttribute;
    }

    public PrimaryAttribute getBasePrimaryAttributes() {
        return basePrimaryAttribute;
    }

    // Sum of total primary attributes for calculations
    public int getTotalSumPrimaryAttributes() {
        int total = totalPrimaryAttribute.getVitality() + totalPrimaryAttribute.getStrength() + totalPrimaryAttribute.getDexterity() + totalPrimaryAttribute.getIntelligence();
        return total;
    }

    // Setting total primary attributes from base attributes and armor-attributes
    public void setTotalPrimaryAttributes() {
        for (Item item : equipment.values()){
            if (item.getSlot() != Slot.WEAPON){
                Armor armor = (Armor) item;
                PrimaryAttribute itemPrimaryAttribute = armor.getPrimaryAttribute();
                totalPrimaryAttribute = new PrimaryAttribute(totalPrimaryAttribute.getVitality() + itemPrimaryAttribute.getVitality(), totalPrimaryAttribute.getStrength() + itemPrimaryAttribute.getStrength(), totalPrimaryAttribute.getDexterity() + itemPrimaryAttribute.getDexterity(), totalPrimaryAttribute.getIntelligence()+ itemPrimaryAttribute.getIntelligence());
            }
        }}

    // Level up that takes in attributes to update the attributes
    public void levelUp(int vitality, int strength, int dexterity, int intelligence){
        // Getting previous for the print
        int previousLevel = this.level;
        PrimaryAttribute previousBasePrimaryAttributes = new PrimaryAttribute(basePrimaryAttribute.getVitality(), basePrimaryAttribute.getStrength(), basePrimaryAttribute.getDexterity(), basePrimaryAttribute.getIntelligence());
        // Updates attributes
        basePrimaryAttribute.setVitality(basePrimaryAttribute.getVitality() + vitality);
        basePrimaryAttribute.setStrength(basePrimaryAttribute.getStrength() + strength);
        basePrimaryAttribute.setDexterity(basePrimaryAttribute.getDexterity() + dexterity);
        basePrimaryAttribute.setIntelligence(basePrimaryAttribute.getIntelligence() + intelligence);
        // Updating level
        this.level = this.level + 1;}

    // Equips both weapon and armor, throws exceptions
    public boolean equip(Slot slot, Item item) throws InvalidWeaponException, InvalidArmorException, InvalidSlotException {
        // Checking if weapon
        if (item instanceof Weapon){
            // Casting to weapon
            Weapon weapon = (Weapon) item;
            // Checks if it's of usable type, if character got required level and if the slot is weapon
            if (usableWeapons.contains(weapon.getType()) && weapon.getRequiredLevel() <= level && slot == Slot.WEAPON){
                // Equips and updates total primary attributes, dps and setting slot
                equipment.put(slot, weapon);
                weapon.setSlot(slot);
                setTotalPrimaryAttributes();
                return true;
            } else{
                // Throws fitting exception with descriptive message
                if (!usableWeapons.contains(weapon.getType())){ throw new InvalidWeaponException("Weapon is of a type that is not allowed for this character");}
                else if (weapon.getRequiredLevel() > level) { throw new InvalidWeaponException("Weapon is too high of a level requirement");}
                else { throw new InvalidSlotException("Weapon can't be placed in other than weapon-slot");}
            }
        }
        // If not weapon, then armor
        else {
            // Casting to armor
            Armor armor = (Armor) item;
            // Checks if it's of usable type, if character got required level and if the slot is not weapon
            if (usableArmors.contains(armor.getType()) && armor.getRequiredLevel() <= level && slot != Slot.WEAPON){
                // Equips and updates total primary attribtues, dps and setting slot
                equipment.put(slot, armor);
                armor.setSlot(slot);
                setTotalPrimaryAttributes();
                return true;
            } else{
                // Throws fitting exception with descriptive message
                if (!usableArmors.contains(armor.getType())){ throw new InvalidArmorException("Armor is of a type that is not allowed for this character");}
                else if (armor.getRequiredLevel() > level) { throw new InvalidArmorException("Armor is too high of a level requirement");}
                else { throw new InvalidSlotException("Armor can't be placed in weapon-slot");}
            }
        }
    }

    // Setting damage per second with given formulas
    public void setDPS(int attribute){
        // Checks if there's any weapon
        if (equipment.containsKey(Slot.WEAPON)) {
            Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
            DPS = weapon.getDamagePerSecond() * (1 + ((double) attribute/100));
        }
        else {
            DPS = 1 * (1 + ((double) getTotalSumPrimaryAttributes()/100));
        }
    }

    // Custom stringbuilder for equipments
    public String equipmentToString(){
        if (equipment == null){
            return "[empty]";
        }
        else{
            return equipment.toString();
        }
    }

    // Custom toString
    @Override
    public String toString() {
        return "\nCharacter:\n\t" +
                "name=" + name + "\n\t" +
                "level=" + level + "\n\t" +
                "basePrimaryAttribute=" + basePrimaryAttribute + "\n\t" +
                "totalPrimaryAttribute=" + totalPrimaryAttribute + "\n\t" +
                "equipment=" + equipmentToString() + "\n\t" +
                "usableWeapons=" + usableWeapons + "\n\t" +
                "usableArmors=" + usableArmors + "\n\t" +
                "DPS=" + DPS;
    }
}
