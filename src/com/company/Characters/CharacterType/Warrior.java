package com.company.Characters.CharacterType;
import com.company.Characters.Character;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Warrior extends Character {
    // Constructor
    public Warrior(String inputName){
        name = inputName;
        // Each Warrior starts at level 1 with attributes 10, 5, 2, 1
        level = 1;
        basePrimaryAttribute = new PrimaryAttribute(10, 5, 2, 1);
        // Setting usable weapons and armors
        usableWeapons = new ArrayList<>(Arrays.asList(WeaponTypes.AXE, WeaponTypes.HAMMER, WeaponTypes.SWORD));
        usableArmors = new ArrayList<>(Arrays.asList(ArmorTypes.MAIL, ArmorTypes.PLATE));
        equipment = new HashMap<>();
        totalPrimaryAttribute = basePrimaryAttribute;
        setDPS();
    }

    // Custom level up
    public void levelUp(){
        levelUp(5, 3, 2,1);
        setDPS();
    }

    public void setDPS(){
        setDPS(totalPrimaryAttribute.getStrength());
    }
}
