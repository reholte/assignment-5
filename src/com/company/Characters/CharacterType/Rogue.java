package com.company.Characters.CharacterType;
import com.company.Characters.Character;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Rogue extends Character {
    // Constructor
    public Rogue(String inputName){
        name = inputName;
        // Each Rogue starts at level 1 with attributes 8,2,6,1
        level = 1;
        basePrimaryAttribute = new PrimaryAttribute(8, 2, 6, 1);
        // Setting usable weapons and armors
        usableWeapons = new ArrayList<>(Arrays.asList(WeaponTypes.DAGGER, WeaponTypes.SWORD));
        usableArmors = new ArrayList<>(Arrays.asList(ArmorTypes.LEATHER, ArmorTypes.MAIL));
        equipment = new HashMap<>();
        totalPrimaryAttribute = basePrimaryAttribute;
        setDPS();
    }

    // Custom level up for Rogue
    public void levelUp(){
        levelUp(3, 1, 4, 1);
        setDPS();
    }

    public void setDPS() {
        super.setDPS(totalPrimaryAttribute.getDexterity());
    }
}
