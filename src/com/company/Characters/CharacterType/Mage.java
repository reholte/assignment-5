package com.company.Characters.CharacterType;
import com.company.Characters.Character;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;

import java.util.*;

public class Mage extends Character {
    // Constructor
    public Mage(String inputName){
        name = inputName;
        // Each Mage starts with level 1 and attributes of 5,1,1,8
        level = 1;
        basePrimaryAttribute = new PrimaryAttribute(5, 1, 1, 8);
        // Setting usable weapons for Mage
        usableWeapons = new ArrayList<>(Arrays.asList(WeaponTypes.STAFF, WeaponTypes.WAND));
        // Setting usable armors for Mage
        usableArmors = new ArrayList<>(Arrays.asList(ArmorTypes.CLOTH));
        equipment = new HashMap<>();
        totalPrimaryAttribute = basePrimaryAttribute;
        setDPS();
    }

    // Custom level up for Mage
    public void levelUp(){
        levelUp(3, 1, 1, 5);
        setDPS();
    }

    public void setDPS() {
        super.setDPS(totalPrimaryAttribute.getIntelligence());
    }
}
