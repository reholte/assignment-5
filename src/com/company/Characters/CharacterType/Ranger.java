package com.company.Characters.CharacterType;
import com.company.Characters.Character;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Ranger extends Character {
    // Constructor
    public Ranger(String inputName){
        name = inputName;
        // Each Ranger starts at level 1 with attributes 8,1,7,1
        level = 1;
        basePrimaryAttribute = new PrimaryAttribute(8, 1, 7, 1);
        // Setting usable weapons and armors
        usableWeapons = new ArrayList<>(Arrays.asList(WeaponTypes.BOW));
        usableArmors = new ArrayList<>(Arrays.asList(ArmorTypes.LEATHER, ArmorTypes.MAIL));
        equipment = new HashMap<>();
        totalPrimaryAttribute = basePrimaryAttribute;
        setDPS();
    }

    // Custom level up for Ranger
    public void levelUp(){
        levelUp(2, 1, 5, 1);
        setDPS();
    }

    public void setDPS(){
        setDPS(totalPrimaryAttribute.getDexterity());
    }
}
