package com.company.Exceptions;

public class InvalidSlotException extends Exception {
    public InvalidSlotException(String message){
        super(message);
    }
}
