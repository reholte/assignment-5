package com.company.Items;

import com.company.PrimaryAttributes.PrimaryAttribute;
import com.company.Slots.Slot;

public abstract class Item {
    // Declarations
    protected String name;
    protected int requiredLevel;
    protected Slot slot;


    // Getters and setters


    public String getName() {
        return name;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

}
