package com.company.Items.ItemType;

import com.company.Items.Item;
import com.company.PrimaryAttributes.PrimaryAttribute;

public  class Armor extends Item {
    // Declarations
    protected ArmorTypes type;
    protected  PrimaryAttribute primaryAttribute;
    // Constructor
    public Armor(String inputName, int level, ArmorTypes type, PrimaryAttribute attributes){
        name = inputName;
        requiredLevel = level;
        this.type = type;
        primaryAttribute = attributes;
    }

    public ArmorTypes getType() {
        return type;
    }


    public PrimaryAttribute getPrimaryAttribute() {
        return primaryAttribute;
    }

    // Custom toString
    @Override
    public String toString() {
        return "Armor{" +
                "name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                ", type=" + type +
                '}';
    }
}
