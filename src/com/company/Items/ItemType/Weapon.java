package com.company.Items.ItemType;

import com.company.Items.Item;
import com.company.PrimaryAttributes.PrimaryAttribute;


public class Weapon extends Item {
    // Declarations
    protected WeaponTypes type;
    protected int damage;
    protected double attackSpeed;
    // Constructor
    public Weapon(String inputName, int level, WeaponTypes type, int inputDamage, double attack){
        name = inputName;
        requiredLevel = level;
        this.type = type;
        damage = inputDamage;
        attackSpeed = attack;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponTypes getType() {
        return type;
    }

    public double getDamagePerSecond(){
        return damage * attackSpeed;
    }

    // Custom toString
    @Override
    public String toString() {
        return "Weapon{" +
                "name='" + name + '\'' +
                ", requiredLevel=" + requiredLevel +
                ", slot=" + slot +
                ", damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                ", type=" + type +
                '}';
    }
}
