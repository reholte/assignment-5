package com.company.Items.ItemType;

public enum WeaponTypes{
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
}