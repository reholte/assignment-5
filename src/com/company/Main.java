package com.company;

import com.company.Characters.Character;
import com.company.Characters.CharacterType.Mage;
import com.company.Characters.CharacterType.Ranger;
import com.company.Characters.CharacterType.Rogue;
import com.company.Characters.CharacterType.Warrior;
import com.company.Exceptions.InvalidArmorException;
import com.company.Exceptions.InvalidSlotException;
import com.company.Exceptions.InvalidWeaponException;
import com.company.Items.Item;
import com.company.Items.ItemType.Armor;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.Weapon;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;
import com.company.Slots.Slot;

public class Main {

    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException, InvalidSlotException {
        Mage mage = new Mage("Harry");
        Ranger ranger = new Ranger("Elisabeth");
        Rogue rogue = new Rogue("Diana");
        Warrior warrior = new Warrior("Andrew");

        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);
        System.out.println(warrior);

        // Updating all characters
        mage.levelUp();
        ranger.levelUp();
        rogue.levelUp();
        warrior.levelUp();

        // Creating slots, armortypes, weapontypes, armor and weapons
        Slot head = Slot.HEAD;
        Slot body = Slot.BODY;
        Slot weapon = Slot.WEAPON;
        Slot legs = Slot.LEGS;

        ArmorTypes leather = ArmorTypes.LEATHER;
        ArmorTypes cloth = ArmorTypes.CLOTH;
        ArmorTypes mail = ArmorTypes.MAIL;
        ArmorTypes plate = ArmorTypes.PLATE;

        WeaponTypes bow = WeaponTypes.BOW;
        WeaponTypes wand = WeaponTypes.WAND;
        WeaponTypes sword = WeaponTypes.SWORD;
        WeaponTypes axe = WeaponTypes.AXE;
        WeaponTypes staff = WeaponTypes.STAFF;
        WeaponTypes dagger = WeaponTypes.DAGGER;
        WeaponTypes hammer = WeaponTypes.HAMMER;

        Armor pants = new Armor("pants", 1, leather, new PrimaryAttribute(1,1,0,0));
        Armor socks = new Armor("socks", 3, cloth,  new PrimaryAttribute(0,1,1,0));
        Armor west = new Armor("west", 2, mail,  new PrimaryAttribute(0, 2, 1, 0));
        Armor helmet = new Armor("helmet", 1, plate,  new PrimaryAttribute(1, 0, 0, 2));

        Weapon bowie = new Weapon("bowie", 1, bow, 4, 2.1);
        Weapon wandie = new Weapon("wandie", 2, wand, 2, 1.1);
        Weapon swordie = new Weapon("swordie", 3, sword, 2, 2.3);
        Weapon axie = new Weapon("axie", 1, axe, 3, 1.8);
        Weapon staffie = new Weapon("staffie", 2, staff, 5, 1.9);
        Weapon daggie = new Weapon("daggie", 2, dagger, 3, 2.4);
        Weapon hammie = new Weapon("hammie", 3, hammer, 2, 3.1);

        // Leveling up some of the characters
        mage.levelUp();
        warrior.levelUp();

        // Trying to equip some armors and weapons to some characters, these will throw exceptions
        tryEquip(mage, weapon, axie);
        tryEquip(mage, body, west);
        tryEquip(ranger, weapon, pants);
        tryEquip(warrior, body, axie);
        tryEquip(rogue, weapon, swordie);

        // Trying to equip some armors and weapons to characters, these will work
        tryEquip(mage, weapon, wandie);
        tryEquip(mage, legs, socks);

        tryEquip(ranger, weapon, bowie);
        tryEquip(ranger, body, west);
        tryEquip(ranger, legs, pants);

        tryEquip(rogue, weapon, daggie);
        tryEquip(rogue, body, west);
        tryEquip(rogue, legs, pants);

        tryEquip(warrior, weapon, hammie);
        tryEquip(warrior, head, helmet);
        tryEquip(warrior, body, west);

        // Printing out all characters
        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);
        System.out.println(warrior);

        // Updating some weapons and leveling up rogue
        rogue.levelUp();
        tryEquip(mage, weapon, staffie);
        tryEquip(rogue, weapon, swordie);
        tryEquip(warrior, weapon, axie);

        // Printing out all characters
        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);
        System.out.println(warrior);


    }
    // Method that tries to equip
    public static void tryEquip(Character character, Slot slot, Item item){
        try {
            character.equip(slot, item);
            if (character instanceof Mage){
                Mage mage = (Mage) character;
                mage.setDPS();
            }
            else if (character instanceof Rogue){
                Rogue rogue = (Rogue) character;
                rogue.setDPS();
            }
            else if (character instanceof Ranger){
                Ranger ranger = (Ranger) character;
                ranger.setDPS();
            }
            else if (character instanceof Warrior){
                Warrior warrior = (Warrior) character;
                warrior.setDPS();
            }
        } catch (InvalidSlotException e){
            System.out.println(e);
        } catch (InvalidArmorException e){
            System.out.println(e);
        }
        catch (InvalidWeaponException e){
            System.out.println(e);
        }
    }
}
