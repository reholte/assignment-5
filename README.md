# Assignment 5 
## Ragnhild Emblem Holte

### Java - RPG Characters 

#### How to run:
**Main:**   
Rightclick Main and press `Run 'Main.main()'`  
**or**  
cd into out/artifacts/assignment_5_jar and execute following command in the terminal: `java -jar assignment-5.jar`  

**CharacterTest:**  
Rightclick CharacterTest and press `Run 'CharacterTest'`  

**ItemTest:**  
Rightclick ItemTest and press `Run 'ItemTest'`  

### Notes:
- I know that it wasn't necessary to check if armor or weapon was placed in the correct slot, but I wanted to. So I also have a custom exception called InvalidSlotException for those cases 
- No need to have a test in ItemTest for checking if armor or weapon is placed in right slot. My if-check in equip() in Character makes it impossible to add to the wrong slot.

### Program description: 
This is an console application. You can create characters and equip them with armor and weapons. 