package com.company.Characters;

import com.company.Characters.CharacterType.Mage;
import com.company.Characters.CharacterType.Warrior;
import com.company.Exceptions.InvalidArmorException;
import com.company.Exceptions.InvalidSlotException;
import com.company.Exceptions.InvalidWeaponException;
import com.company.Items.Item;
import com.company.Items.ItemType.Armor;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.Weapon;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;
import com.company.Slots.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void levelUp_void_ShouldIncreaseLevelBy1() {
        // Arrange
        Mage mage = new Mage("Harry");
        int expected = mage.getLevel() + 1;
        // Act
        mage.levelUp();
        // Assert
        assertEquals(expected, mage.getLevel());
    }

    @Test
    void levelUp_void_ShouldIncreaseBasePrimaryAttributes() {
        // Arrange
        Mage mage = new Mage("Harry");
        PrimaryAttribute expected = new PrimaryAttribute(mage.getBasePrimaryAttributes().getVitality() + 3, mage.getBasePrimaryAttributes().getStrength() + 1, mage.getBasePrimaryAttributes().getDexterity() + 1, mage.getBasePrimaryAttributes().getIntelligence() + 5);
        // Act
        mage.levelUp();
        PrimaryAttribute actual = mage.getBasePrimaryAttributes();
        // Assert
        assertTrue(expected.equals(actual));
    }

    @Test
    void levelUp_void_ShouldIncreaseTotalPrimaryAttributes() {
        // Arrange
        Mage mage = new Mage("Harry");
        PrimaryAttribute expected = new PrimaryAttribute(mage.getTotalPrimaryAttribute().getVitality() + 3, mage.getTotalPrimaryAttribute().getStrength() + 1, mage.getTotalPrimaryAttribute().getDexterity() + 1, mage.getTotalPrimaryAttribute().getIntelligence() + 5);
        // Act
        mage.levelUp();
        PrimaryAttribute actual = mage.getTotalPrimaryAttribute();
        // Assert
        assertTrue(expected.equals(actual));
    }

    @Test
    void equip_ValidArmor_ShouldReturnTrue() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException{
        // Arrange
        Mage mage = new Mage("Harry");
        Slot body = Slot.BODY;
        Item mageCloth = new Armor("mageCloth", 1, ArmorTypes.CLOTH,  new PrimaryAttribute(1, 0, 1, 0));
        // Act
        boolean actual = mage.equip(body, mageCloth);
        // Assert
        assertTrue(actual);
    }

    @Test
    void equip_ValidWeapon_ShouldReturnTrue() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot weapon = Slot.WEAPON;
        Item mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        // Act
        boolean actual = mage.equip(weapon, mageWand);
        // Assert
        assertTrue(actual);

    }

    @Test
    void equip_ValidWeapon_ShouldUpdateTotalPrimaryAttributes() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot weapon = Slot.WEAPON;
        int vitality = 1;
        int strength = 0;
        int dexterity = 1;
        int intelligence = 0;
        Item mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        PrimaryAttribute expected = new PrimaryAttribute(mage.getBasePrimaryAttributes().getVitality() + vitality, mage.getBasePrimaryAttributes().getStrength() + strength, mage.getBasePrimaryAttributes().getDexterity() + dexterity, mage.getBasePrimaryAttributes().getIntelligence() + intelligence);
        // Act
        mage.equip(weapon, mageWand);
        PrimaryAttribute actual = mage.getTotalPrimaryAttribute();
        // Assert
        assertTrue(expected.equals(actual));
    }


    @Test
    void equip_InvalidArmor_ShouldThrowException() {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot head = Slot.HEAD;
        Item mageMail = new Armor("mageMail", 1, ArmorTypes.MAIL, new PrimaryAttribute(1, 0, 1, 0));
        String expectedMessage = "Armor is of a type that is not allowed for this character";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> mage.equip(head, mageMail)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void equip_InvalidWeapon_ShouldThrowException() {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot weapon = Slot.WEAPON;
        Item mageBow = new Weapon("mageBow", 1, WeaponTypes.BOW, 1, 1.1);
        String expectedMessage = "Weapon is of a type that is not allowed for this character";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> mage.equip(weapon, mageBow)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void equip_InvalidSlotArmor_ShouldThrowException() {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot weapon = Slot.WEAPON;
        Item mageCloth = new Armor("mageCloth", 1, ArmorTypes.CLOTH, new PrimaryAttribute(1, 0, 1, 0));
        String expectedMessage = "Armor can't be placed in weapon-slot";
        // Act
        Exception exception = assertThrows(InvalidSlotException.class, () -> mage.equip(weapon, mageCloth)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void equip_InvalidSlotWeapon_ShouldThrowException() {
        // Arrange
        Mage mage = new Mage("Harry");
        Slot body = Slot.BODY;
        Item mageStaff = new Weapon("mageStaff", 1, WeaponTypes.STAFF, 1, 1.1);
        String expectedMessage = "Weapon can't be placed in other than weapon-slot";
        // Act
        Exception exception = assertThrows(InvalidSlotException.class, () -> mage.equip(body, mageStaff)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void equip_TooHighRequiredLevelWeapon_ShouldThrowException() {
        // Arrange
        Warrior warrior = new Warrior("Andrew");
        Slot weapon = Slot.WEAPON;
        Item warriorAxe = new Weapon("warriorAxe", 2, WeaponTypes.AXE, 1, 1.1);
        String expectedMessage = "Weapon is too high of a level requirement";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> warrior.equip(weapon, warriorAxe)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void equip_TooHighRequiredLevelArmor_ShouldThrowException() {
        // Arrange
        Warrior warrior = new Warrior("Andrew");
        Slot body = Slot.BODY;
        Item warriorPlate = new Armor("warriorPlate", 2, ArmorTypes.PLATE, new PrimaryAttribute(1, 0, 1, 0));
        String expectedMessage = "Armor is too high of a level requirement";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> warrior.equip(body, warriorPlate)
        );
        String actualMessage = exception.getMessage();
        // Assert
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void setDPS_WithWeapon_ShouldCorrectlyCalculateDPS() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException{
        // Arrange
        Mage mage = new Mage("Harry");
        Slot weapon = Slot.WEAPON;
        Item mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        mage.equip(weapon, mageWand);
        Weapon weapon0 = (Weapon) mage.getEquipment().get(Slot.WEAPON);
        double expected = weapon0.getDamagePerSecond() * (1 + ((double) mage.getTotalSumPrimaryAttributes()/100));
        // Act
        mage.setDPS();
        // Assert
        assertEquals(expected, mage.getDPS());
    }

    @Test
    void setDPS_WithArmor_ShouldCorrectlyCalculateDPS() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException{
        // Arrange
        Mage mage = new Mage("Harry");
        Slot body = Slot.BODY;
        Item mageCloth = new Armor("mageCloth", 1, ArmorTypes.CLOTH, new PrimaryAttribute(1, 0, 1, 0));
        mage.equip(body, mageCloth);
        double expected = 1 * (1 + ((double) mage.getTotalSumPrimaryAttributes()/100));
        // Act
        mage.setDPS();
        // Assert
        assertEquals(expected, mage.getDPS());
    }

    @Test
    void setDPS_WithArmorAndWeapon_ShouldCorrectlyCalculateDPS() throws InvalidWeaponException, InvalidArmorException, InvalidSlotException{
        // Arrange
        Mage mage = new Mage("Harry");
        Slot body = Slot.BODY;
        Item mageCloth = new Armor("mageCloth", 1, ArmorTypes.CLOTH, new PrimaryAttribute(1, 0, 1, 0));
        mage.equip(body, mageCloth);
        Slot weapon = Slot.WEAPON;
        Item mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        mage.equip(weapon, mageWand);
        Weapon weapon1 = (Weapon) mage.getEquipment().get(Slot.WEAPON);
        double expected = weapon1.getDamagePerSecond() * (1 + ((double) mage.getTotalSumPrimaryAttributes()/100));
        // Act
        mage.setDPS();
        // Assert
        assertEquals(expected, mage.getDPS());
    }

    @Test
    void setDPS_WithoutWeaponOrArmor_ShouldCorrectlyCalculateDPS() {
        // Arrange
        Mage mage = new Mage("Harry");
        double expected = 1 * (1 + ((double) mage.getTotalSumPrimaryAttributes()/100));
        // Act
        mage.setDPS();
        // Assert
        assertEquals(expected, mage.getDPS());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}