package com.company.Items;

import com.company.Items.ItemType.Armor;
import com.company.Items.ItemType.ArmorTypes;
import com.company.Items.ItemType.Weapon;
import com.company.Items.ItemType.WeaponTypes;
import com.company.PrimaryAttributes.PrimaryAttribute;
import com.company.Slots.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void setSlot_WeaponInWeaponSlot_ShouldAddToRightSlot() {
        // Arrange
        Slot weapon = Slot.WEAPON;
        Weapon mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        Slot expected = weapon;
        // Act
        mageWand.setSlot(weapon);
        // Assert
        assertEquals(expected, mageWand.getSlot());
    }

    @Test
    void setSlot_ArmorInArmorSlot_ShouldAddToRightSlot() {
        // Arrange
        Slot body = Slot.BODY;
        Armor mageCloth = new Armor("mageCloth", 1, ArmorTypes.CLOTH, new PrimaryAttribute(1, 0, 1, 0));
        Slot expected = body;
        // Act
        mageCloth.setSlot(body);
        // Assert
        assertEquals(expected, mageCloth.getSlot());
    }

    @Test
    void getDamagePerSecond_void_ShouldCalculateCorrectly() {
        // Arrange
        Weapon mageWand = new Weapon("mageWand", 1, WeaponTypes.WAND, 1, 1.1);
        double expected = mageWand.getDamage() * mageWand.getAttackSpeed();
        // Act
        double actual = mageWand.getDamagePerSecond();
        // Assert
        assertEquals(expected, actual);
    }
}